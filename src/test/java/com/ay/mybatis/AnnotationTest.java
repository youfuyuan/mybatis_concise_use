package com.ay.mybatis;

import com.ay.mybatis.annotion.service.QueryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/* *
 * Created by Ay on 2018/11/30
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AnnotationTest {
    
    @Autowired
    private QueryService service;
    
    @Test
    public void test(){
        System.out.println(service.query());
    }
}
