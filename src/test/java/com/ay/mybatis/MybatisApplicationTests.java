package com.ay.mybatis;

import com.ay.mybatis.dao.AddressMapper;
import com.ay.mybatis.dao.UserMapper;
import com.ay.mybatis.entity.Address;
import com.ay.mybatis.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MybatisApplicationTests {

    @Autowired
    private AddressMapper addressMapper;

    @Autowired
    private UserMapper userMapper;
    @Test
    public void contextLoads() {
//        List<Address> addresses = addressMapper.getAll();
//        for(Address address : addresses){
//            System.out.println(address.getProvince());
//        }
        User user = userMapper.findUserWithAll(4L);
        System.out.println(user.toString());
    }

}
