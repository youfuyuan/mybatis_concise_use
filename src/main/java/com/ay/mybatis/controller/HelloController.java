package com.ay.mybatis.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/* *
 * Created by Ay on 2018/11/25
 */
@RestController
public class HelloController {
    @RequestMapping("/")
    public String index(){
        return "1";
    }
}
