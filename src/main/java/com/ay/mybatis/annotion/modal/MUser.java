package com.ay.mybatis.annotion.modal;

import org.springframework.stereotype.Component;

/* *
 * Created by Ay on 2018/11/30
 */
@Component
public class MUser {
    /*  用户id    */
    private Integer id;
    
    /*  用户姓名    */
    private String name;
    
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
}
