package com.ay.mybatis.annotion.modal;

import com.ay.mybatis.annotion.annotation.SetValue;
import com.ay.mybatis.annotion.dao.MUserMapper;
import org.springframework.stereotype.Component;

/* *
 * Created by Ay on 2018/11/30
 */
@Component
public class Order {
    /*  订单id    */
    private Integer id;
    
    /*  下订单的用户id    */
    private Integer customerId;
    
    /*  下订单的用户名     */
    @SetValue(className = MUserMapper.class, methodName = "queryNameById", paraNameForGet = "customerId", paraNameForSet = "customerName")
    private String customerName;
    
    @SetValue(className = MUserMapper.class,methodName = "queryAgeById",paraNameForGet = "customerId",paraNameForSet = "customerAge")
    private Integer customerAge;
    
    public Integer getCustomerAge() {
        return customerAge;
    }
    
    public void setCustomerAge(Integer customerAge) {
        this.customerAge = customerAge;
    }
    
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public Integer getCustomerId() {
        return customerId;
    }
    
    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }
    
    public String getCustomerName() {
        return customerName;
    }
    
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
}
