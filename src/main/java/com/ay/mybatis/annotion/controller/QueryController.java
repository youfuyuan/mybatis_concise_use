package com.ay.mybatis.annotion.controller;

import com.ay.mybatis.annotion.service.QueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/* *
 * Created by Ay on 2018/11/30
 */
@RestController
public class QueryController {
    
    @Autowired
    private QueryService queryService;
    
    @RequestMapping("/query/")
    public Object query(){
        return queryService.query();
    }
}
