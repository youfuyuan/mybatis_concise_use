package com.ay.mybatis.annotion.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/* *
 * Created by Ay on 2018/11/30
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SetValue {
    
    /**
     * 查询类的类名
     * @return
     */
    Class<?> className();
    
    /**
     * 查询的方法
     * @return
     */
    String methodName();
    
    /**
     * 借助的字段
     * @return
     */
    String paraNameForGet();
    
    
    /**
     * 设置的字段
     * @return
     */
    String paraNameForSet();
}
