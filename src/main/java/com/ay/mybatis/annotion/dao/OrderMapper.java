package com.ay.mybatis.annotion.dao;

import com.ay.mybatis.annotion.modal.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/* *
 * Created by Ay on 2018/11/30
 */
@Mapper
public interface OrderMapper {

    @Select("select * from morder")
    @Results(@Result(property = "customerId",column = "customeId"))
    List<Order> queryOrder();
}
