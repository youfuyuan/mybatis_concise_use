package com.ay.mybatis.annotion.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/* *
 * Created by Ay on 2018/11/30
 */
@Mapper
public interface MUserMapper {
    
    @Select("select name from user where id=#{id}")
    String queryNameById(@Param("id") Integer id);
    
    @Select("select age from user where id=#{id}")
    Integer queryAgeById(@Param("id") Integer id);
}
