package com.ay.mybatis.annotion.service;

import com.ay.mybatis.annotion.annotation.NeedSetFieldValue;
import com.ay.mybatis.annotion.dao.MUserMapper;
import com.ay.mybatis.annotion.dao.OrderMapper;
import com.ay.mybatis.annotion.modal.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/* *
 * Created by Ay on 2018/11/30
 */
@Service
public class QueryService {

    @Autowired
    private OrderMapper orderMapper;
    
    @Autowired
    private MUserMapper userMapper;
    
    @NeedSetFieldValue  //需要自动注入参数值
    public List<Order> query(){
        
        //先查询所有订单
        List<Order> orderList = orderMapper.queryOrder();
        
        //侵入代码
        //根据订单的用户id去查询用户名
//        for(Order order : orderList){
//            String customerName = userMapper.queryNameById(order.getCustomerId());
//            order.setCustomerName(customerName);
//        }
//
        
        return orderList;
    }
}
