package com.ay.mybatis.annotion.aspect;

import com.ay.mybatis.annotion.utils.BeanUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

/* *
 * Created by Ay on 2018/11/30
 */
@Component
@Aspect
public class SetValueAnnotationAspect {
    @Autowired
    private BeanUtil beanUtil;

    @Around("@annotation(com.ay.mybatis.annotion.annotation.NeedSetFieldValue)")
    public Object setValueAround(ProceedingJoinPoint joinPoint){
        System.out.println("-----------------环绕前置通知--------------------");
        try {
            Object o =joinPoint.proceed();
            //返回类型是个集合
            if(o instanceof Collection){
                boolean result = beanUtil.setFieldValue((Collection)o);
                if(!result){
                    System.out.println("设置Field值不成功");
                }
            }else{
                System.out.println("返回类型不是集合");
            }
            System.out.println("-----------------环绕后置通知--------------------");
            return o;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        
        return null;
    }
}
