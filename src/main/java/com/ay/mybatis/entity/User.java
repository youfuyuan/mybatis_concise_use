package com.ay.mybatis.entity;

import org.springframework.stereotype.Component;

import java.util.List;

/* *
 * Created by Ay on 2018/11/24
 */
@Component
public class User {
    private Long id;
    private String nickName;
    //地址信息，和用户是一对一的关系
    private Address address;
    //地址id
    private Long addressId;
    //用户拥有的车，和用户是一对多的关系
    private List<Car> cars;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", nickName=" + nickName +
                ", address=" + address.getProvince()+
                ", addressId=" + addressId +
                ", cars=" + cars +
                '}';
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }
}
