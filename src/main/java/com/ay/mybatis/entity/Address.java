package com.ay.mybatis.entity;

import org.springframework.stereotype.Component;

/* *
 * Created by Ay on 2018/11/24
 */
@Component
public class Address {

    private Long id;

    private String province;

    private String city;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}