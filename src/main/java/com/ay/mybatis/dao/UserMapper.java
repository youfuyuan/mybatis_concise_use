package com.ay.mybatis.dao;

import com.ay.mybatis.entity.User;
import org.apache.ibatis.annotations.*;

/* *
 * Created by Ay on 2018/11/24
 */
@Mapper
public interface UserMapper {
    /**
     * 一对一查询，根据id找用户，再根据addressId找Address
     * 因为生产环境中的实体类属性名跟数据库的字段名可能不一样
     * 所以@Result进行映射
     * 对于一对一查询，借助附属查询方法，此时的@Result语义是
     * 使用数据库中column的参数进行附属方法查询，返回给property
     * @param id
     * @return
     */
    @Select("SELECT * FROM `user` where id = #{id}")
    @Results({
            @Result(property = "id",column = "id"),
            @Result(property = "addressId",column = "address_id"),
            @Result(property = "nickName",column = "nick_name"),
            @Result(property = "address",column = "address_id",
            one = @One(select = "com.ay.mybatis.dao.AddressMapper.findAddressById")),
            @Result(property = "cars",column = "id",
            many = @Many(select = "com.ay.mybatis.dao.CarMapper.findCarByUserId"))
    })
    User findUserWithAll(Long id);
}
