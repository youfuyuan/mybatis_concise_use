package com.ay.mybatis.dao;

import com.ay.mybatis.entity.Address;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/* *
 * Created by Ay on 2018/11/24
 */
@Mapper
public interface AddressMapper {
    @Select("select * from address")
    List<Address> getAll();

    /**
     * 根据地址id查询地址
     */
    @Select("SELECT * FROM `address` WHERE id = #{id}")
    Address findAddressById(Long id);
}
