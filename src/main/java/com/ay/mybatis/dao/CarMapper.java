package com.ay.mybatis.dao;

import com.ay.mybatis.entity.Car;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/* *
 * Created by Ay on 2018/11/24
 */
@Mapper
public interface CarMapper {
    /**
     * 根据用户id查询所有的车
     */
    @Select("SELECT * FROM `car` WHERE user_id = #{userId}")
    @Results(@Result(property = "userId",column = "user_id"))
    List<Car> findCarByUserId(Long userId);
}
